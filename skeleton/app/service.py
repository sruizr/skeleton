from skeleton.app.persons import PersonId


class Service:
    def __init__(self, store, queries):
        self._store = store
        self._queries = queries

    def say_hello(self, user_name):
        person = self._store.load(PersonId(user_name))
        return person.greet()

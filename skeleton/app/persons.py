from dataclasses import dataclass

from . import Entity, Id


@dataclass(frozen=True)
class PersonId(Id):
    """Person unique identification."""

    username: str


class Person(Entity):
    """Person with information of application of user."""

    def __init__(self, user_name: str):
        self.id = PersonId(user_name)
        self.user_name = user_name
        self.firstname = None
        self.surname = None

    def identify(self, full_name: str):
        """Set natural name to person."""
        surname, firstname = full_name.split(", ")

        self.surname = surname
        self.firstname = firstname

    def greet(self):
        """Say hello givin him name."""
        return f"Hello, I'm {self.firstname}, nice to meet you!"

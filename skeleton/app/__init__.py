from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Type


@dataclass(frozen=True)
class Id:
    pass


class Entity:
    def __eq__(self, other):
        return self.id == other.id


class Store(ABC):
    @abstractmethod
    def load(self, entity_id: Type[Id]) -> Entity:
        ...

    @abstractmethod
    def save(self, entity: Entity):
        ...

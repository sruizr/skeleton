from typing import Any

from skeleton.app.persons import Person
from skeleton.app.service import Service
from skeleton.persistence import PersistenceFactory


class Main:
    """Inject main hexagonal components for starting sofware."""

    application: Service | None = None
    drivers: dict[str, Any] = {}
    drivens: dict[str, Any] = {}

    def __init__(self, environment: str = "testing"):
        if self.application:
            return

        self.environment = environment
        persistence_fry = PersistenceFactory(environment)

        self.drivens = {
            "store": persistence_fry.create_store(),
            "queries": persistence_fry.create_queries(),
        }
        self._load_entity_examples()

        self.application = Service(**self.drivens)

        self.drivers = {"rest": None}

    def _load_entity_examples(self):
        person = Person("sruiz")
        person.identify("Ruiz, Salvador")
        self.drivens["store"].save(person)

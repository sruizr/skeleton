from typing import Type

from skeleton.app import Entity, Id, Store


class PersistenceFactory:
    """Create persistence layer depending on environment."""

    def __init__(self, environment):
        self._environment = environment

    def create_store(self):
        """Create store suitable for environment."""
        return OnmemStore()

    def create_queries(self):
        """Create query suitable for environment."""
        return {}


class OnmemStore(Store):
    """Fake store on RAM memory for testing purpose."""

    def __init__(self):
        self._data = {}

    def load(self, entity_id: Type[Id]):
        """Load entity from its identificator value."""
        return self._data[entity_id]

    def save(self, entity: Entity):
        """Persist the entity."""
        self._data[entity.id] = entity

from fastapi import Depends, FastAPI

from skeleton.app.service import Service
from skeleton.config import Main


def application() -> Service:
    return Main("staged").application


rest_server = FastAPI()


@rest_server.get("/hello/{username}")
async def hello_world(username: str, app=Depends(application)):
    """User greets, saying hello"""
    return {"greet": app.say_hello(username)}

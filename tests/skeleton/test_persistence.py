from unittest.mock import MagicMock

from skeleton.persistence import OnmemStore


class A_OnmemStore:
    def should_persist_domain_entities(self):
        store = OnmemStore()
        entity = MagicMock()

        store.save(entity)
        other = store.load(entity.id)

        assert other == entity

import json
from unittest.mock import MagicMock

from fastapi.testclient import TestClient

from skeleton.config import Main
from skeleton.rest import application, rest_server


class MockedApp:
    def __init__(self, server):
        self._server = server
        self._main = MagicMock(spec=Main)

    def _service(self):
        return self._main.application

    def __enter__(self):
        self._server.dependency_overrides[application] = self._service
        return self._main.application

    def __exit__(self, exec_type, exc_value, exc_traceback):
        self._server.dependency_overrides = {}


class A_RestServer:
    def setup_method(self):
        self.client = TestClient(rest_server)

    def should_say_hello_depending_on_user(self):
        with MockedApp(rest_server) as app:
            app.say_hello.return_value = "Hello everybody"
            print(app)

            response = self.client.get("/hello/sruiz")

            assert response.json() == {"greet": "Hello everybody"}
            app.say_hello.assert_called_with("sruiz")

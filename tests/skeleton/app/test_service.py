from unittest.mock import MagicMock

from skeleton.app.persons import PersonId
from skeleton.app.service import Service


class A_Service:
    def should_transfer_hello_from_person(self):
        person = MagicMock()
        person.greet.return_value = "Hello, I am a person"
        store = MagicMock()
        store.load.return_value = person
        queries = MagicMock()

        service = Service(store, queries)

        assert service.say_hello("sruiz") == "Hello, I am a person"
        store.load.assert_called_with(PersonId("sruiz"))

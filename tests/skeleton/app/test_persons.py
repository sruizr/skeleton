from skeleton.app.persons import Person, PersonId


class A_Person:
    def should_be_unique_by_its_username(self):
        me = Person("sruiz")
        other = Person("other")

        assert me != other

        again_me = Person("sruiz")
        assert me == again_me

        assert PersonId("sruiz") == me.id

    def should_be_identified_by_fullname(self):
        me = Person("sruiz")
        me.identify("Ruiz Romero, Salvador")

        assert me.firstname == "Salvador"
        assert me.surname == "Ruiz Romero"

    def should_greet(self):
        me = Person("sruiz")
        me.identify("Ruiz Romero, Salvador")

        assert me.greet() == "Hello, I'm Salvador, nice to meet you!"

from unittest.mock import patch

from skeleton.config import Main


class A_Main:
    def setup_method(self):
        self._persistence_fry_patch = patch("skeleton.config.PersistenceFactory")
        self.PersistenceFactory = self._persistence_fry_patch.start()
        self._person_patch = patch("skeleton.config.Person")
        self.Person = self._person_patch.start()

    def teardown_method(self):
        self._persistence_fry_patch.stop()
        self._person_patch.stop()

    def should_load_example_data_at_testing_environment(self):
        main = Main("testing")

        store = main.drivens["store"]
        store.save.assert_called_with(self.Person.return_value)

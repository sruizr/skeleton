from skeleton.app.persons import Person
from skeleton.config import Main


class UseCase_PersonSayHello:
    def scenario_a_known_persons_say_hello(self):
        main = Main()
        store = main.drivens["store"]

        person = Person("sruiz")
        person.identify("Ruiz Romero, Salvador")
        store.save(person)

        app = main.application
        expected = "Hello, I'm Salvador, nice to meet you!"
        assert app.say_hello("sruiz") == expected

# Skeleton

Skeleton for very basic microservice with API Rest and Persistence.

## Description

Following concepts like, hexagonal architecture and domain driven design the structure of modules inside the application is defined.

Here yo have a picture of the general structure:
![hexagonal architecture](/docs/arquitecture.svg)

The `app` module contains the application layer and inside you have domain object moduless or service modules. The `app` layer defines its own ports for driven adapters. At this moment the only adapter is the persistence layer, with CQRS structure, with the following ports:

1. `Store` for loading and saving domain aggregates.
2. `Queries` for containing all queries to persistence adapter

On the other hand, there is the `tests` directory where you have a correct structure for `BDD` and `TDD` working:

1. `tests/skeleton`: Insid this directory should be a identical distribution of application modules. The final models should start with `test_module.py` so pytest will find it as testing files. A tipical aproach is to define for each external class `Example` a class test with name `A_Example` and methods which should start with `should_`. Pytest will follow these template and run these methods as tests.
2. `tests/acceptance`: Inside this directory should be a play list of files like `feat_what_ever_description.py`. Every test class should start with `UseCase_` and contains `scenario_*` so pytest can find it.
3. `tests/integration`: Inside there is tests to adapters for assuring that they work on staging / production environment.

## Installation

With the help of application poetry, it's really easy to install once you have cloned the project from gitlab. So it's necessary to have installed in your system poetry (see [here](https://python-poetry.org/docs/#installation) for the proper installation).

Once it is installed, run the following code:

`poetry install`

This command will install a virtual environment with all the necessary dependencies.

## Usage

Following BDD, the procedure to generate production code should be:

1. Create an acceptance test at `tests/acceptance`.
2. Doing several loops, create necessary tdd cicles at `tests/skeleton`:
   1. Create unit test
   2. Create code to pass unit test.
   3. Refactor
   4. Assert it is still ok after refactor.

### Scripts of common use

In the directory `deploy` there are the following scripts done in bash:

1. `test-units.sh`: Run the unit tests of the software giving coverage range. More than 90% of coverage is a good measurement. It should be ok before any commit.
2. `test-acceptance.sh`: Run the acceptance tests. They can be run on development environment and should be all ok before a push to remote.
3. `test-integration.sh`: To be run only on stage environments.
4. `run-on-dev.sh`: This command open the application on testing environment, locally to development environment.
